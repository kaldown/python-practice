#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import sys
import re
import os
import shutil
import commands

"""Copy Special exercise
"""

# +++your code here+++
# Write functions and modify main() to call them

def specials(*dirs): #get tuple of dirs
	def paths(dir):
		filenames = os.listdir(dir)
		
		special = re.findall(r'\w*__\w+__[\w\.]*', ' '.join(filenames))
		paths = [os.path.abspath(os.path.join(dir, x)) for x in special]
		return paths
	res = []
	for d in dirs:
		res.append(paths(d))
	result = [x for j in res for x in j]
	return result

def to_zip(paths, zip_path):
	pass #won't do this under windows

def cp_to(paths, dir_path):
	if not os.path.exists(dir_path): os.mkdir(dir_path)
	for p in paths:
		shutil.copy(p, dir_path)


		

def main():
  # This basic command line argument parsing code is provided.
  # Add code to call your functions below.

  # Make a list of command line arguments, omitting the [0] element
  # which is the script itself.
  args = sys.argv[1:]
  if not args:
    print "usage: [--todir dir][--tozip zipfile] dir [dir ...]";
    sys.exit(1)

  # todir and tozip are either set from command line
  # or left as the empty string.
  # The args array is left just containing the dirs.
  todir = ''
  if args[0] == '--todir':
    todir = args[1]
    del args[0:2]

  tozip = ''
  if args[0] == '--tozip':
    tozip = args[1]
    del args[0:2]

  if len(args) == 0:
    print "error: must specify one or more dirs"
    sys.exit(1)

  # +++your code here+++
  # Call your functions

  if todir: cp_to(specials(*args), todir)
  elif tozip: zip_to(specials(*args), tozip)
  else: print(specials(*args))

if __name__ == "__main__":
  main()
