Yolanda 391
Yosef 836
Yoselin 600
Yousef 998
Yuliana 862
Yurem 1000
Yuridia 894
Yusuf 800
Yvette 342
Yvonne 371
Zachariah 305
Zachary 12
Zachery 180
Zack 884
Zackary 232
Zackery 306
Zaid 924
Zaiden 898
Zain 797
Zaire 798
Zakary 683
Zana 809
Zander 279
Zane 222
Zaniyah 882
Zara 774
Zaria 479
Zariah 581
Zavier 742
Zayden 488
Zayne 825
Zechariah 655
Zhane 585
Zion 232
Zoe 122
Zoey 119
Zoie 552
Wrote profile results to babynames.py.lprof
Timer unit: 1e-06 s

Total time: 0.070623 s
File: babynames.py
Function: extract_names at line 37

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    37                                           @profile
    38                                           def extract_names(filename):
    39                                             """
    40                                             Given a file name for baby.html, returns a list starting with the year string
    41                                             followed by the name-rank strings in alphabetical order.
    42                                             ['2006', 'Aaliyah 91', Aaron 57', 'Abagail 895', ' ...]
    43                                             """
    44                                             # +++your code here+++
    45         1           14     14.0      0.0    f = open(filename)
    46         1          625    625.0      0.9    whole = f.read()
    47         1            2      2.0      0.0    f.close
    48                                           
    49         1         1005   1005.0      1.4    year = re.search(r'>[\w+\s]+(\d\d\d\d)\s*<', whole)
    50         1        16196  16196.0     22.9    tuples = re.findall(r'>(\d+)</td><td>(\w+)</td><td>(\w+)<', whole)
    51                                           
    52         1            3      3.0      0.0    names = []
    53         1            2      2.0      0.0    names_list = {}
    54                                           
    55         1            6      6.0      0.0    names.append(year.group(1))
    56                                           
    57     10001         9927      1.0     14.1    for rank, mans, girls in tuples:
    58     10000        12433      1.2     17.6        if mans not in names_list or rank < names_list[mans]:
    59      3843         4258      1.1      6.0            names_list[mans] = rank
    60     10000        12380      1.2     17.5        if girls not in names_list or rank < names_list[girls]:
    61      4139         4370      1.1      6.2            names_list[girls] = rank
    62                                           
    63         1         2446   2446.0      3.5    names_buff = sorted(names_list.keys())
    64      3002         2819      0.9      4.0    for name in names_buff:
    65      3001         4136      1.4      5.9        names.append(name + ' ' + names_list[name])
    66                                           
    67         1            1      1.0      0.0    return names

Total time: 0.110065 s
File: babynames.py
Function: main at line 69

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    69                                           @profile
    70                                           def main():
    71                                             # This command-line parsing code is provided.
    72                                             # Make a list of command line arguments, omitting the [0] element
    73                                             # which is the script itself.
    74         1            2      2.0      0.0    args = sys.argv[1:]
    75                                           
    76         1            2      2.0      0.0    if not args:
    77                                               print('usage: [--summaryfile] file [file ...]')
    78                                               sys.exit(1)
    79                                           
    80                                             # Notice the summary flag and remove it from args if it is present.
    81         1            1      1.0      0.0    summary = False
    82         1            2      2.0      0.0    if args[0] == '--summaryfile':
    83                                               summary = True
    84                                               del args[0]
    85                                             # +++your code here+++
    86         2            3      1.5      0.0    for names in args:
    87         1       109689 109689.0     99.7      name = extract_names(names)
    88         1            2      2.0      0.0      if summary:
    89                                                   f = open(names + '.summary', 'w')
    90                                                   f.write('\n'.join(name))
    91                                                   f.close()
    92                                               else:
    93         1          364    364.0      0.3          print('\n'.join(name))

