def task(func, *seq):
    """
    The function receives a function and N sequences (lists/tuples).
    All sequences have the same length M.
    The number of arguments of the func is equal to the number of sequences.
    Return a list of [res1, res2, ..., resM], where:

    res1 = func(seq1[1], seq2[1], ..., seqN[1])
    res2 = func(seq1[2], seq2[2], ..., seqN[2])
    ...
    resM = func(seq1[M], seq2[M], ..., seqN[M])
    """
    # BEGIN
    result = []
    transpose = list(zip(*seq))
    for i in transpose:
      result.append(func(*list(i)))
    return result


lam = lambda x, y, z: x * y - z
assert task(lam, [6, 5, 1], [8, 22, 2], [6, 1, 1]) == [42, 109, 1]

#lamt = [6, 5, 1, 55, 1, 14], [8, 22, 2, 7, 12, 3], [6, 1, 1, 5, 5, 5]
#trans = list(zip(*lamt))
#[(lambda x, y, z: x*y-z)(x,y,z) for (x,y,z) in trans]
#[42, 109, 1, 380, 7, 37]
