import pdb

def task(users, funcs):
    """
    The function receives a list of dictionaries like [{field1->value1, field2->value2},...]
    and a dictionary of functions like field-->function-with-one-argument.
    For each user, correct it's values by calling corresponding function with the value of a field.

    Usage:
    >>> task([{'name': 'ivan'}], {'name': lambda name: name.upper()})
    [{'name': 'IVAN'}]
    """
    # BEGIN
    for user in users:
      for k, v in funcs.items():
        if user.get(k) != None: 
          user[k] = v(user[k])
    
    return user


users = [
        {'name': 'ivan', 'age': 29},
        {'id': 42, 'age': 31},
        {'smokes': False, 'friends': ['john', 'yan', 'amy']},
    ]
funcs = {
        'smokes': lambda x: not(x),
        'name': lambda x: x.capitalize(),
        'friends': lambda x: sorted(x),
        'age': lambda x: x * 2,
    }    
assert task(users, funcs) == [
        {'age': 58, 'name': 'Ivan'},
        {'age': 62, 'id': 42},
        {'friends': ['amy', 'john', 'yan'], 'smokes': True},
    ]


