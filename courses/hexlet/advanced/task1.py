# -*- encoding: utf-8 -*-

from teacher.exceptions import *
from teacher.funcs import except_function

def solution(value):
    # BEGIN
    try:
      t = type(value)
      if t == list: 
        raise Task1ListException()
      elif t == int:
        raise Task1IntException(-1)
      elif t == dict:
        raise Task1DictException({'ok': False, 'error': value})
      elif t == set:
        raise Task1SetException(None)
      elif t == int:
        raise Task1IntException(-1)
      elif t == str:
        raise Task1StrException(value)
      elif t == None:
        raise Task1NoneException(None)
    # END
