# -*- encoding: utf-8 -*-


class Task1ListException(Exception):
    pass


class Task1DictException(Exception):
    pass


class Task1SetException(Exception):
    pass


class Task1IntException(Exception):
    pass


class Task1StrException(Exception):
    pass


class Task1NoneException(Exception):
    pass


class Task3Random1Exception(Exception):
    pass


class Task3Random2Exception(Exception):
    pass

