# -*- encoding: utf-8 -*-

import random

from teacher.exceptions import *


def except_function(value):
    if isinstance(value, str):
        raise Task1StrException('Hi, how are you? I fine')
    elif isinstance(value, list):
        raise Task1ListException()
    elif isinstance(value, set):
        raise Task1SetException()
    elif isinstance(value, dict):
        raise Task1DictException('Catch me if you can')
    elif isinstance(value, int):
        raise Task1IntException()
    else:
        raise Task1NoneException()


def random_exception():
    res = random.choice([Task3Random1Exception, Task3Random2Exception, None])
    if res is not None:
        raise res()

