#!/usr/bin/python

scope = dict(pusher=2, alph='abcdefghijklmnopqrstuvwxyz')

def alpher(l, pusher=scope['pusher'], alph=scope['alph']):
    letter = l.lower()
    try:
        assert isinstance(pusher, int)
        assert pusher >= 0
    except AssertionError:
        print 'pusher %s is bad value' % pusher

    if alph.find(letter) > (len(alph) - 1) - pusher:
        result = alph[(alph.find(letter) + pusher) % (len(alph)-1)-1]
    else: result = alph[alph.find(letter) + pusher]
    if l.islower(): return result
    else: return result.upper()

#some_text = """g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."""

def solution(text):
    result = ''.join([alpher(l) if l.lower() in scope['alph'] else l for l in text])
    return result

#====UTests====

assert solution('') == ''
assert solution('wbzyx #!&') == 'ydbaz #!&'
assert solution('WbZYx #!&') == 'YdBAz #!&'

