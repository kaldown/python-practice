#!/usr/bin/python2.7

class Merge:
    def __init__(self, arr):
        self.arr = arr
        self.i = 0
        self.merge_sort(self.arr)

    def merge(self, a, b):
        c = []
        while a and b:
            if a[-1] <= b[-1]:
                c.append(b.pop())
            else:
                # to show coordinates
                #dominant = a[-1]
                #[self.i.append((dominant, x)) for x in b]

                inversions = len(b)
                self.i += inversions
                c.append(a.pop())
        c.extend(reversed(a))
        c.extend(reversed(b))
        return list(reversed(c))
        
    def merge_sort(self, arr):
        ln = len(arr)//2
        l = arr[:ln]
        r = arr[ln:]
        if len(arr) <= 2:
            return self.merge(l, r)
        else:
            a, b = self.merge_sort(l), self.merge_sort(r)
            res = self.merge(a, b)
            return res

#with open("IntegerArray.txt") as f:
#        f = f.read()
#array = map(int, f.split())

