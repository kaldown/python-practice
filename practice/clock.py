def angle(time):
    hour, minute = time.split(':')
    hour = int(hour) % 12
    minute = int(minute)
    hour_rad = 30  # 90 / 3 (3 hours in every quarter)
    minute_rad = 6  # 90 / 15
    def hour_shift(minute):
        return minute / 60. * hour_rad
    minute_uni = minute_rad * minute
    hour_uni = hour_rad * hour + hour_shift(minute)
    result = max(minute_uni, hour_uni) - min(minute_uni, hour_uni)
    return result if result <= 180 else abs(180 - result)


# test
def test():
    with open('clock_test.txt', 'r') as afile:
        for line in afile:
            input, expected = line.split()
            result = angle(input)
            if not float(expected) == result:
                raise Exception('angle(%s) is not %s, but %s' % 
                            (input, expected, result)
                        )
test()

