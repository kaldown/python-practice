# -*- coding: utf-8 -*-

from selenium import webdriver
import codecs
import time

#### DEFINE ####

names = {
         ' -167px;': 'choco',
         ' -668px;': 'choco with nuts',
         ' -835px;': 'orange', 
         ' -501px;': 'green',
         ' 0px;'   : 'roasted orange',
         ' -334px;': 'green hat',
        }

names2 = {k:v for v,k in names.items()}

################

promos = dict()

url = "http://www.givetwo.ru/promo/"
driver = webdriver.Remote("http://localhost:4444/wd/hub", desired_capabilities=webdriver.DesiredCapabilities.HTMLUNITWITHJS)
#driver = webdriver.Firefox()
driver.get(url)
element = driver.find_element_by_xpath("/html/body/div[1]/div[6]/div/div[7]/div[2]/a")

def push():
    element.click()
    time.sleep(5)
    m1_check = driver.find_element_by_xpath("/html/body/div[1]/div[6]/div/div[3]/div")
    m2_check = driver.find_element_by_xpath("/html/body/div[1]/div[6]/div/div[4]/div")
    m3_check = driver.find_element_by_xpath("/html/body/div[1]/div[6]/div/div[5]/div")

    m1_style = m1_check.get_attribute("style").split(":")[1]
    m2_style = m2_check.get_attribute("style").split(":")[1]
    m3_style = m3_check.get_attribute("style").split(":")[1]

    if m1_style == m2_style and m1_style == m3_style:
        return m1_style, driver.find_element_by_xpath("/html/body/div[1]/div[6]/div/div[7]/div[1]/strong").text
    else: 
        print m1_style, m2_style, m3_style

def find_as_much(how_much):
    assert how_much <= 6
    while len(promos) != how_much:
        res = push()
        if res != None:
            key = names[res[0]]
            promos[key] = res[1]

    with codecs.open('find_as_much.txt', 'a', encoding='utf8') as f:
        for k, v in promos.items():
            f.write('{0}\{1}'.format(str(k), str(v)))

def find_certain(what_to_find):
    assert isinstance('what_to_find', str)
    what_to_find = names2[what_to_find]
    while True:
        res = push()
        if res[0] == what_to_find:
            with codecs.open('{0}.txt'.format(what_to_find), 'a', encoding='utf8') as f:
                f.write('{0}'.format(res[1]))

#find_as_much(6)
for x in range(50):
    push()

driver.quit()

