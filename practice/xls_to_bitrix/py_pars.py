# -*- coding: utf-8 -*-

import codecs
import re

with codecs.open("t1.csv", "r", encoding="utf8") as f:
    with codecs.open("t2.csv", "w+", encoding="utf8") as f1:

        top = f.readline()
        f1.write(top)
        line_gen = (line for line in f if len(line.split(';')) >= 6)
        for line in line_gen:
            res = line.split(';')
            if len(res) == 7:
                #import pdb; pdb.set_trace()
                tmp = f.readline()
                tmp = tmp.split(';')[1:]
                res[-1] = res[-1].rstrip()[1:]
                res += tmp
            st = res[3]
            fio = res[5]

            phone = res[6]

            email = res[-1] #email
            site = res[-2] #site

            if len(site) < 5 : res[-2] = ';'
            if len(email) < 5 : res[-1] = ';\n'

            res[5] = ';'.join(fio.split())
            #print st
            pattern = re.compile(ur'г\.\s(\w+)', re.UNICODE)
            city = pattern.search(st)
            if city:
                res[3] = city.group(1)
                f1.write(';'.join(res))
            else:
                res[3] = ''
                f1.write(';'.join(res))

with open('t2.csv', 'r') as r:
    n = 1
    r.readline()
    for line in r:
        to_test = line.split(';')[0]
        assert n == int(to_test)
        n += 1
