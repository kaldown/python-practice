import dis
import timeit

#@profile
def direct_call(l):
    newlist = []
    [newlist.append(x) for x in range(l)]
    newlist.append(l)
    return newlist

#@profile
def store_call(l):
    newlist = []
    imp = newlist.append
    [imp(x) for x in range(l)]
    imp(l)
    return newlist

l = 100000

@profile
def setup1(): 
    newlist = store_call(l)
    newlist.pop(0)

@profile
def setup2(): 
    newlist = store_call(l)
    newlist.pop()

setup1()
setup2()
