#!/usr/bin/env python3.4

@profile
def list_merge1(keys, values):
    dict_merge = {}
    while len(keys): 
        key = keys.pop(0)
        if len(values):
            dict_merge[key] = values.pop(0)
        else:
            dict_merge[key] = None
    return dict_merge

@profile
def list_merge2(keys, values):
    dict_merge = {}
    keys = list(reversed(keys))
    values = list(reversed(values))
    #import pdb; pdb.set_trace()
    while len(keys): 
        key = keys.pop()
        if len(values):
            dict_merge[key] = values.pop()
        else:
            dict_merge[key] = None
    return dict_merge


k1 = [x for x in range(50000)]
v1 = [x for x in range(50000) if x % 5 != 0]

list_merge1(k1, v1)

k2 = [x for x in range(50000)]
v2 = [x for x in range(50000) if x % 5 != 0]

list_merge2(k2, v2)

# TODO:
# try dict_*
# should change yandex task_1.py
