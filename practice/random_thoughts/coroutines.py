#!/usr/bin/python

import asyncio

#@asyncio.coroutine
#def say_name(name):
#    while True:
#        print(name)
#        yield from asyncio.sleep(1)
#
#tasks = [
#    asyncio.ensure_future(say_name('kaldown')),
#    asyncio.ensure_future(say_name('bogdan'))]
#
#loop = asyncio.get_event_loop()
#loop.run_until_complete(asyncio.wait(tasks))
#loop.close()

async def say_name(name, wait_long=1):
    while True:
        print(name)
        await asyncio.sleep(wait_long)

tasks = [
    asyncio.ensure_future(say_name('kaldown', 5)),
    asyncio.ensure_future(say_name('bogdan'))]

loop = asyncio.get_event_loop()
loop.run_until_complete(asyncio.wait(tasks))
loop.close()

#@asyncio.coroutine
#def factorial(name, number):
#    f = 1
#    for i in range(2, number+1):
#        print("Task %s: Compute factorial(%s)..." % (name, i))
#        yield from asyncio.sleep(1)
#        f *= i
#    print("Task %s: factorial(%s) = %s" % (name, number, f))
#
#loop = asyncio.get_event_loop()
#tasks = [
#    asyncio.ensure_future(factorial("A", 2)),
#    asyncio.ensure_future(factorial("B", 3)),
#    asyncio.ensure_future(factorial("C", 4))]
#loop.run_until_complete(asyncio.wait(tasks))
#loop.close()
