#http://www.smashcompany.com/technology/embarrassing-code-i-wrote-under-stress-at-a-job-interview
# random task from internet

import sys

def happy(num):
	try:
	    num = str(num)
	except Exception as e:
	    print('Error', e)
	    sys.exit(1)


	result = [int(i) ** 2 for i in num]
	result = sum(result)

	return result


def is_happy(num):
	if happy(num) == 1:
		return True
	else: 
		return False

print(is_happy(happy(31)))
