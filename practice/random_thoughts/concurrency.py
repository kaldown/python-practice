#!/usr/bin/python

from multiprocessing import Pool
import time

start_time = time.time()

def f(x):
    return x*x

with Pool(5) as p:
    (p.map(f, range(10000000)))

#(list(map(f, range(10000000))))

print("Timer: %f" % (time.time() - start_time))
