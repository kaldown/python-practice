import re

container = {
    'parenthes': 0,
    'bracket': 0,
    'brace': 0,
}

def close(element_type):
    if container[element_type] == 0:
        raise Exception('closing more than opening')
    container[element_type] -= 1

def open(element_type):
    container[element_type] += 1

parenthes_map = {
    ']': (close, 'bracket'),
    ')': (close, 'parenthes'),
    '}': (close, 'brace'),
    '[': (open, 'bracket'),
    '(': (open, 'parenthes'),
    '{': (open, 'brace'),
}

s = 'some(bla)blasl][, no this is (b)labl)e {skajdklsfj} {ksalfj)'
#s = 'some(bla)blasl[], no this is (b)labl()e {skajdklsfj} {ksalfj}'

r = re.compile(r'[\(\)\[\]\{\}]+?')

result = r.findall(s)

for elem_type in result:
    func, element = parenthes_map[elem_type]
    func(element)

