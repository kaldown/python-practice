def get_pivot(arr):
    return arr[0]

def qsort(source_arr):
    if len(source_arr) < 2:
        return source_arr

    pivot = get_pivot(source_arr)

    lower = []
    greater = []
    for elem in source_arr:
        if elem < pivot:
            lower.append(elem)
        elif elem > pivot:
            greater.append(elem)

    result = []
    result.extend(qsort(lower))
    result.append(pivot)
    result.extend(qsort(greater))

    return result

assert qsort([3,4,2,5,1]) == [1,2,3,4,5]
