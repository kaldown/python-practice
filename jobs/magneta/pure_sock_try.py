import socket
from json import dumps
from time import sleep
from random import choice
import re
from base64 import b64encode
from hashlib import sha1


# https://tools.ietf.org/html/rfc6455
WEBSOCK_GUID = '258EAFA5-E914-47DA-95CA-C5AB0DC85B11'
KEY_RE = r'\nSec-WebSocket-Key:\s?(.*)\r\n'


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('127.0.0.1', 4444))
s.listen(1)


def is_response_ends(chunk):
    if chunk[-4:] == '\r\n\r\n':
        return True
    return False


def get_first_response(client_socket):
    chunks = []
    while True:
        chunk = client_socket.recv(1000)
        chunks.append(chunk)
        if is_response_ends(chunk):
            return u''.join(chunks)


def retreive_key(first_response):
    
    if first_response:
        key = re.search(KEY_RE, first_response)
        if key:
            key = key.groups()[0]
            print('recieve %s' % key)
            return key
    print('got no key, bad websocket, bad!')
    exit(1)


def prepare_key(key):
    digest = sha1(key + WEBSOCK_GUID).digest()
    return b64encode(digest)
    

def send_data(sock, data):
    """ send all data to the client socket

    returns bool in equivalent to success of operation

    rtype: bool
    """
    # EOT
    data += '\n\n'
    #import pdb; pdb.set_trace()
    try:
        sock.sendall(data)
    except socket.error as e:
        return False

    return True

def handle_handshake(client_socket):

    first_response = get_first_response(client_socket)
    key = retreive_key(first_response)
    prepared_key = prepare_key(key)

    handshake_response = (
        'HTTP/1.1 101 Switching Protocols\r\n'\
        'Upgrade: websocket\r\n'\
        'Connection: Upgrade\r\n'\
        'Sec-WebSocket-Accept: %s\r\n\r\n'
    ) % prepared_key
    
    return handshake_response.decode()
    
    
def generate_data(data=None):
    """ dummy data generator

    rtype: str
    """
    data = {
        'flag': choice((0, 1)),
        'data': 'some data to be send' if data is None else data
    }

    return dumps(data)


def serve_conn():
    """ main endpoint to serve connection

    blocking example
    """
    client_socket, addr = s.accept()
    print('connected %s' % (addr,))
    
    prepared_headers = handle_handshake(client_socket)
    # ACK
    send_data(client_socket, prepared_headers)
    #send_data(client_socket, handshake_response)

    #for _ in range(2):
    #    data = generate_data()
    #    send_data(client_socket, data)
    #    print('%s sended' % data)


def main():
    while True:
        serve_conn()


if __name__ == '__main__':
    main()

