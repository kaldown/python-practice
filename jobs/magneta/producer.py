#!/usr/bin/env python

import asyncio
from random import shuffle
import websockets
from string import ascii_lowercase, digits

ALPHANUM_MAP = list(ascii_lowercase) + list(digits)


async def shuffle_data():
    alphanum = ALPHANUM_MAP[:]
    shuffle(alphanum)
    random_data = ''.join(alphanum)[:10]
    return random_data


async def push(websocket, path):
    while True:
        random_data = await shuffle_data()
        await websocket.send(random_data)
        await asyncio.sleep(1)

start_server = websockets.serve(push, '0.0.0.0', 4444)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
