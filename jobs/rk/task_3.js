var json = {
        'err_code': 0,
        'err_msg': 'OK',
        'list': false,
        'data': [{
            'pa': '827',
            'balance': '123.45',
            'charge_list': [
                    {'id': 1, 'number': '00001', 'date': '2013-01-01', 'sum': '1000.00'},
                    {'id': 2, 'number': '00002', 'date': '2013-01-15', 'sum': '1000.00'},
                    {'id': 3, 'number': '00015', 'date': '2013-01-31', 'sum': '1200.00'},
            ],
            'payment_list': [
                    {'id': 1, 'number': '00121', 'date': '2013-01-01', 'sum': '1300.00'},
                    {'id': 2, 'number': '00342', 'date': '2013-01-17', 'sum': '100.00'},
                    {'id': 3, 'number': '00435', 'date': '2013-02-04', 'sum': '3200.00'},
            ]
        }]
    };


$(json.data).each(function(idx){
    var div_id = "<div id=" + idx + ">" + "</div>";
    $(".base").append(div_id);
    $(".base > #" + idx).append("<div class='bills'></div>");
    $(".base > #" + idx).append("<div class='payments'></div>");
    $(".base > #" + idx).append("<div class='charges'></div>");

    $("#" + idx + "> .bills").append("<li>" + "Счет: " + this.pa + "</li>");
    $("#" + idx + "> .bills").append("<li>" + "Баланс: " + this.balance + "</li>");
    $("#" + idx + "> .bills").append("<br>");

    $("#" + idx + "> .charges").append("<button>" + "Счета" + "</button>");
    $("button:last").click(function(){
        $("#" + idx + "> .charges > table").slideToggle("slow");
    });
    $("#" + idx + "> .payments").append("<button>" + "Платежи" + "</button>");
    $("button:last").click(function(){
        $("#" + idx + "> .payments > table").slideToggle("slow");
    });
    $("#" + idx + "> .charges").append("<table><tr><th>ид</th><th>номер</th><th>дата</th><th>сумма</th></tr></table>");
    $("#" + idx + "> .payments").append("<table><tr><th>ид</th><th>номер</th><th>дата</th><th>сумма</th></tr></table>");
    $(this.charge_list).each(function(){
        $(".charges > table > tbody").append("<tr></tr>");
        $(".charges > table > tbody > tr:last").append("<td>" + this.id + "</td>");
        $(".charges > table > tbody > tr:last").append("<td>" + this.number + "</td>");
        $(".charges > table > tbody > tr:last").append("<td>" + this.date + "</td>");
        $(".charges > table > tbody > tr:last").append("<td>" + this.sum + "</td>");
        
    });
    $(this.payment_list).each(function(){
        $(".payments > table > tbody").append("<tr></tr>");
        $(".payments > table > tbody > tr:last").append("<td>" + this.id + "</td>");
        $(".payments > table > tbody > tr:last").append("<td>" + this.number + "</td>");
        $(".payments > table > tbody > tr:last").append("<td>" + this.date + "</td>");
        $(".payments > table > tbody > tr:last").append("<td>" + this.sum + "</td>");
    });
    $("#" + idx + "> .charges").append("<br>");
    $("#" + idx + "> .payments").append("<br>");
});

