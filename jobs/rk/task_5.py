#!/usr/bin/python

#@profile
def fib1(n):
    if n == 1 or n == 0: return n
    else: return fib1(n-1) + fib1(n-2)

#@profile
def fib2(n, cache = {0:0, 1:1}):
    if n in cache: return cache[n]
    if n < 2: return cache[n]
    else: 
        t = fib2(n-1) + fib2(n-2)
        cache[n] = t
    return cache[n]

#@profile
def fib3(n):
    def fib3_fun(a, b, count):
        if count == 0: return a
        return fib3_fun(a+b, a, count-1)
    return fib3_fun(0, 1, n)

n = 20
fib1(n)
fib2(n)
fib3(n)
