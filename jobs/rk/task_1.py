#!/usr/bin/python

# http://regexr.com/

import re
import sys
import time
import datetime

# date = %d/%m/%Y
# time = %H:%M:%S

def call(filename):

    with open(filename) as f:
        text = f.read()

    ips = re.compile(r'\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b', re.MULTILINE)
    times = re.compile(r'\b(?:(?:2[0-4]|[01]?[0-9])\:)(?:[0-5]?[0-9]\:)[0-5]?[0-9]\b', re.MULTILINE)
    dates = re.compile(r'\b[0-3]?[0-9]\/(?:1[0-2]|[0-9])\/\d{4}\b', re.MULTILINE)
    bits = [int(i) for i in text.split() if i.isdigit()]
        

    def cmp(ip_tuple):
        '''
        compare ip in ip_tuple, so lower will always left
        '''
        ip1 = ip_tuple[0]
        ip2 = ip_tuple[1]

        l1 = ip1.split('.')
        l2 = ip2.split('.')

        for i in range(len(l1)):
	    if int(l1[i]) < int(l2[i]): return (ip1, ip2)
	    elif int(l1[i]) == int(l2[i]): continue
	    else: return (ip2, ip1)
            break

        return (ip1, ip2)

        

    def make_ip_tuples(lst):
        """
        get list of all ips
        return tuples from each line

        """ 
        try:
            assert len(lst) % 2 == 0
        except AssertionError:
            print 'lst length not even, try to add some IPs to make couples'
            sys.exit(1)
        
        result = []
        for i in range(len(lst))[::2]:
            ip_tuple = (lst[i], lst[i+1])
            res = cmp(ip_tuple)
            result.append(res)
        return result


    def time_stamp(d, t):
        ts = '%s %s' % (d, t)
        result = time.mktime(datetime.datetime.strptime(ts, '%d/%m/%Y %H:%M:%S').timetuple())
        return result


    def make_output(input_list):
        """
        return dict with all columns in sorted order without dublicates
        """
        sorted_list = sorted(input_list, reverse=True, key=lambda x: time_stamp(x[0], x[1]))

        result = []
        for i in sorted_list:
            rows = [row[2] for row in result]
            if i[2] not in rows:
                result.append(list(i))
            else:
                idx = [row[2] for row in result].index(i[2])
                result[idx][3] += i[3]
        return result


    ips = ips.findall(text)
    time_list = times.findall(text)
    date_list = dates.findall(text)
    ip_tuples = make_ip_tuples(ips)

    inp = zip(date_list, time_list, ip_tuples, bits)

    output = make_output(inp)

    return output

    

def main():
    arg = sys.argv[1:]

    if not arg:
        print 'choose a file'
        sys.exit(1)

    filename = arg[0]
    
    output = call(filename)

    for i in output:
        print '%10s %10s %14s %14s %10s' % (i[0], i[1], i[2][0], i[2][1], i[3])


if __name__ == '__main__':
    main()

