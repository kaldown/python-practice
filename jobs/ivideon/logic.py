import struct
from flashlight import FlashLight
from functools import wraps


def singleton(cls):
    ''' Use class as singleton. '''
 
    cls.__new_original__ = cls.__new__
 
    @wraps(cls.__new__)
    def singleton_new(cls, *args, **kw):
        it =  cls.__dict__.get('__it__')
        if it is not None:
            return it
 
        cls.__it__ = it = cls.__new_original__(cls, *args, **kw)
        it.__init_original__(*args, **kw)
        return it
 
    cls.__new__ = singleton_new
    cls.__init_original__ = cls.__init__
    cls.__init__ = object.__init__
 
    return cls


@singleton
class ManageFlashLight(object):
    _ON = b'\x12'
    _OFF = b'\x13'
    _CHC = b'\x20'

    def __init__(self):
        self._fl = FlashLight()

    def _get_type_value(self, data):
        atype, length = struct.unpack('!sH', data[:3])
        value = struct.unpack('!%ii' % length, data[3:3+(length*4)])
        return atype, value

    def _call_command(self, atype, value):
        if atype == self._ON:
            self._fl.turn_on()

        if atype == self._OFF:
            self._fl.turn_off()

        if atype == self._CHC:
            self._fl.set_color(value)

        return self._fl.render()

    def handle_command(self, data):
        atype, value = self._get_type_value(data)
        return self._call_command(atype, value)
