from base64 import b16decode

from tornado import gen
from tornado.iostream import StreamClosedError
from tornado.tcpserver import TCPServer
import tornado.ioloop
import tornado.web

from logic import ManageFlashLight


class FlashLightServer(TCPServer):
    @gen.coroutine
    def handle_stream(self, stream, address):
        while True:
            try:
                data = yield stream.read_until(b"\n")
                data = b16decode(data[:-2])
                ManageFlashLight().handle_command(data)
            except StreamClosedError:
                break


server = FlashLightServer()
server.bind(9999)
server.start(0)
tornado.ioloop.IOLoop.current().start()
