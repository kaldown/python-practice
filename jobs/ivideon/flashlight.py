class FlashLight(object):
    _image = """
      /^\ 
      |#|
     |===|
      |0|
      | |
     =====
    _||_||_
    -------
    """

    def __init__(self, is_on=False, color=(255, 255, 255)):
        self._is_on = is_on
        self._color = color

    def turn_on(self):
        self._is_on = True

    def turn_off(self):
        self._is_on = False

    def set_color(self, color):
        self._color = color

    def render(self):
        return self._render()

    def _render(self):
        if self._is_on:
            start_color = ''
            end_color = ''
            if self._color == (255, 0, 0):  # red
                start_color = '\033[31m'
                end_color = '\033[0m'
            if self._color == (0, 128, 0):  # green
                start_color = '\033[92m'
                end_color = '\033[0m'
            if self._color == (0, 0, 255):  #blue
                start_color = '\033[34m'
                end_color = '\033[0m'
                
            return ''.join([start_color, self._image, end_color])
        return ''
