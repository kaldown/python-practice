import struct
from logic import ManageFlashLight
from flashlight import FlashLight

#### manage test
assert ManageFlashLight()._fl._is_on == False

p = struct.pack('!sH', ManageFlashLight()._ON, 0)
ManageFlashLight().handle_command(p)
assert ManageFlashLight()._fl._is_on

p = struct.pack('!sH', ManageFlashLight()._OFF, 0)
ManageFlashLight().handle_command(p)
assert ManageFlashLight()._fl._is_on == False


#### fl test
fl = FlashLight()

assert fl._is_on == False
assert fl.render() == ''

fl.turn_on()
assert fl._is_on == True
assert fl.render() == fl._image

fl.set_color((255, 0, 0))
assert fl.render() not in (fl._image, '')
