#!/usr/bin/python3

import argparse
from pathlib import Path
from itertools import chain
import requests


EXTENSIONS = ('jpg', 'img',)
URL = 'http://exampleserver.com/images'

def get_files(folder_path):
    """get list of files in given dir

    rtype: Path
    """
    dir = Path(folder_path).expanduser()
    files = chain.from_iterable(dir.glob('*.{}'.format(ext))
            for ext in EXTENSIONS)
    return list(files)

def upload_files(files):
    """upload files to a server from URL constant
    raise on bad status_code

    rtype: None
    """
    r = requests.post(
        URL,
        files=[('image', (afile.name, afile.open()), 'image/' + afile.suffix)
                for afile in files],
    )
    r.raise_for_status()

def main(dir):
    files = get_files(dir)
    upload_files(files)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='image uploader')

    parser.add_argument('--dir', metavar='~/Images', default='./',
			type=Path, help='path to dir with images')
    args = parser.parse_args()
    
    main(args.dir)
