import argparse
from os import path
from datetime import date
from calendar import month_name
from threading import Thread
#from pathlib import Path
import time
import gevent.monkey

from lxml import html
import requests

MAIN_URL = 'https://www.smashingmagazine.com/'
# ADD_URL = f'{year}/{month}/desktop-wallpaper-calendars-{Month}-{year_calc}/'
ADD_URL = '{year}/{month}/desktop-wallpaper-calendars-{Month}-{year_calc}/'
WITH_CALENDAR = 2
WITHOUT_CALENDAR = 3


def is_year_valid(year):
    """ validate year

    :type year: int
    :rtype:  int
    """
    year = int(year)
    today_year = date.today().year
    if 2000 <= year <= today_year:
        return year
    raise argparse.ArgumentTypeError("%s is not between 2000 and %s" % (year,
                                                                        today_year))


def is_month_valid(month):
    """ validate month

    :type month: int
    :rtype: int
    """
    month = int(month)
    if 1 <= month <= 12:
        return month
    raise argparse.ArgumentTypeError("%s is not between 1 and 12" % month)


def calendar_validator(with_calendar):
    """ validate if need calendar in images

    :type resolution: str
    :rtype: unicode
    """
    if with_calendar.lower() == 'yes':
        return WITH_CALENDAR
    return WITHOUT_CALENDAR


def build_resol(resolution):
    """ return url_parse need resolution

    :type resolution: str
    :rtype: unicode
    """
    return u'\xd7'.join(resolution.split('x'))


def get_wallps_urls(month, year, resolution, with_calendar):
    """ get all possible wallp urls by resolution

    :type month: int
    :type year: int
    :return: list(str)
    """
    urls = []

    month_calc = month_name[month % 12 + 1].lower()
    # if it is a december (12) => calculated year must be incremented
    year_calc = year + 1 if month == 12 else year
    # number month should be like '04'
    month = str(month).zfill(2)

    r = requests.get(MAIN_URL + ADD_URL.format(
        month=month, year=year, Month=month_calc, year_calc=year_calc
    ))

    if r.status_code == 200:
        tree = html.fromstring(r.content)
        urls.extend(tree.xpath(u"//html/body/main//div[contains(@class, 'col')\
                                                and contains(@class, 'main')]\
                            /article[contains(@class, 'tag-wallpapers')]\
                            /ul[position() > 3]\
                            /li[position()={with_calendar}]\
                            /a[text()='{resolution}']/@href".format(
            with_calendar=with_calendar,
            resolution=resolution)))
    """
    tree.xpath("//html/body/main//div[@id='content']")
    """
    return urls


def fetch_by_urls(urls):
    """ download images given urls
    and save in file

    :type urls: list(str)
    :return: None
    """
    if isinstance(urls, list):
        for url in urls:
            fname = url.split('/')[-1]
            #path = Path('./%s' % fname)
            if path.exists('./%s' % fname):
                print('already got %s' % fname)
                continue
            print('start fetching %s image' % url)
            r = requests.get(url, stream=True)
            if r.status_code == 200:
                with open(fname, 'wb') as f:
                    for chunk in r:
                        f.write(chunk)
    else:
        # don't look at this :D
        fetch_by_urls([urls])


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='wallpapers downloader')
    parser.add_argument('--month', metavar='4', type=is_month_valid,
                        help='choose a month number', required=True,)
    parser.add_argument('--year', metavar='2017', type=is_year_valid,
                        help='choose a year number', required=True)
    parser.add_argument('--resolution', metavar='800x600', type=build_resol,
                        help='choose a resolution', required=True)
    parser.add_argument('--with_calendar', metavar='YES', default='YES',
                        type=calendar_validator, help='if with calendar')
    parser.add_argument('--threads', action='store_true',
                        help='use threading')

    args = parser.parse_args()

    started = time.time()
    urls = get_wallps_urls(month=args.month,
                           year=args.year,
                           resolution=args.resolution,
                           with_calendar=args.with_calendar)
    if urls:
        if args.threads:
            print('via Threading')
            fetch_time = time.time()
            print('urls took %.1f' % (time.time() - started))

            threads = []
            for url in urls:
                t = Thread(target=fetch_by_urls, args=(url,))
                t.start()
                threads.append(t)
            for thread in threads:
                thread.join()

            ended = time.time()
            print('images took %.1f' % (ended - fetch_time))
            print('total spent %.1f' % (ended - started))

        else:
            print('via gevent')
            fetch_time = time.time()
            print('urls took %.1f' % (time.time() - started))

            gevent.monkey.patch_all()
            jobs = [gevent.spawn(fetch_by_urls, url) for url in urls]
            gevent.wait(jobs)

            ended = time.time()
            print('images took %.1f' % (ended - fetch_time))
            print('total spent %.1f' % (ended - started))

        print('done')
    else:
        print('something wrong')
        exit(1)
