#!/usr/bin/sh

# find top 10 ip with most request using shell, and python. This for sh

awk '{print $1}' $1 | egrep '^[[:digit:]]{1,3}\.[[:digit:]]{1,3}\.[[:digit:]]{1,3}\.[[:digit:]]{1,3}$' | sort -n | uniq -c  | sort | tail -10| awk '{print $2}'
