#!/usr/bin/env python3.4


def login_test1(login):
    import re
    if 1 < len(str(login)) < 20:
        match = re.search(r'^[a-zA-Z][\w\d.-]*[\w]$', login)
        if match: result = match.group()
        else: result = 'nope'
    else: result = 'nope'
    return result


def login_test2(login):
    if str(login)[0].isalpha() and str(login)[-1].isalnum():
        if str(login).isalnum(): return str(login)
        else: 
            login_arr = [i for i in str(login)]
            if '.' in login_arr or '-' in login_arr:
                while '.' in login_arr:
                    login_arr.remove('.')
                while '-' in login_arr:
                    login_arr.remove('-')
                login_new = ''.join(i for i in login_arr)
                if str(login_new).isalnum(): return str(login)
                else: return 'nope'
            else: return 'nope'
    else: return 'nope'


def test(got, expected):
    if got == expected: result = 'Ok'
    else: result = 'Not'
    print('%s\n got:\n%s \n%s\n expected\n' % (result, repr(got), repr(expected)))


def main():

    print('====login_test1:====')
    test(login_test1('kAldi12345'), 'kAldi12345')
    test(login_test1('4kA.ldi.12--345'), 'nope')
    test(login_test1('kA.ldi.12--345'), 'kA.ldi.12--345')
    test(login_test1('kA.l@di.12--345'), 'nope')

    print('====login_test2:====')
    test(login_test2('kAldi12345'), 'kAldi12345')
    test(login_test2('4kA.ldi.12--345'), 'nope')
    test(login_test2('kA.ldi.12--345'), 'kA.ldi.12--345')
    test(login_test2('kA.l@di.12--345'), 'nope')

             
if __name__ == '__main__':
    main()
