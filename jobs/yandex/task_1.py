#!/usr/bin/env python3.4


def list_merge(keys, values):
    dict_merge = {}
    while len(keys): 
        key = keys.pop(0)
        if len(values):
            dict_merge[key] = values.pop(0)
        else:
            dict_merge[key] = None
    return dict_merge

    
def test(got, expected):
    if got == expected: result = 'Ok'
    else: result = 'Not'
    print('%s\n got:\n%s \n%s\n expected\n' % (result, repr(got), repr(expected)))


def main():

    print('====list_merge:====')
    test(list_merge([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], ['a', 'b', 'c', 'd']),
            {1: 'a', 2: 'b', 3: 'c', 4: 'd', 5: None, 6: None,
                7: None, 8: None, 9: None, 10: None})


    test(list_merge([1, 2, 3], ['a', 'b', 'c', 'd']),
            {1: 'a', 2: 'b', 3: 'c'})



if __name__ == '__main__':
    main()
