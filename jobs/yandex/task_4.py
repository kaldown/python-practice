#!/usr/bin/python3.4

#Find top 10 ip with most request using sh and python, this for python 3.4
#P.S. under Windows encoding='cp1251', delete this if got problem with enc.

import re

with open('big_data', 'r', encoding='cp1251') as f:
    data = f.read()
match = re.findall(r'^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s', data,
re.MULTILINE)

dict = {}
for i in match:
   if i in dict: dict[i] += 1
   else: dict[i] = 1

def sort_key(values): return values[1]
ips = sorted(dict.items(), key=sort_key, reverse=True)

for i in ips[:10]:
    print(i[0])
