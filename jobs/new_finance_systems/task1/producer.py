import socket
from json import dumps
from time import sleep
from random import choice


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('localhost', 4444))
s.listen(1)


def send_data(sock, data):
    """ send all data to the client socket

    returns bool in equivalent to success of operation

    rtype: bool
    """
    # EOT
    data += '\n\n'
    import pdb; pdb.set_trace()
    try:
        sock.sendall(data)
    except socket.error as e:
        return False

    return True

    
def generate_data(data=None):
    """ dummy data generator

    rtype: str
    """
    data = {
        'flag': choice((0, 1)),
        'data': 'some data to be send' if data is None else data
    }

    return dumps(data)


def serve_conn():
    """ main endpoint to serve connection

    blocking example
    """
    client_socket, addr = s.accept()
    print('connected %s' % (addr,))
    
    while True:
        data = generate_data()
        if not send_data(client_socket, data):
            print('disconnect %s' % (addr,))
            client_socket.close()
            break
        sleep(1)
    

def main():
    while True:
        serve_conn()


if __name__ == '__main__':
    main()

