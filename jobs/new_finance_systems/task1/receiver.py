import socket
from json import loads


STOP_FLAG_LIMIT = 3

# redo to class
STOP_FLAG_COUNT = 0

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('localhost', 4444))


def to_stop(data):
    """ function to close socket if got 1,1,1 flags in a row

    rtype: bool
    """
    
    global STOP_FLAG_COUNT

    flag = int(data.get('flag'))
    if flag is None:
        print('no flag key in received data')
        return True

    # assumint we got only 1 OR 0
    if not flag:
        # clear counter if got zero
        STOP_FLAG_COUNT = 0
    else:
        STOP_FLAG_COUNT += flag

    if STOP_FLAG_COUNT == STOP_FLAG_LIMIT:
        print('logger STOP')
        return True

    return False


def receive_data(sock):
    """ method to parse data from socket

    rtype: str
    """
    while True:
        data = sock.recv(1024) 

        # check EOT
        if data[-2:] == '\n\n':
            break

    return data


def serve_conn(sock):
    """ main endpoint to serve connection

    blocking example
    """
    data = receive_data(sock)

    if not data:
        print('no data -> close conn')
        sock.close()
        return False

    try:
        # think about receive data as a single object, not an array of
        data = loads(data)
    except ValueError as e:
        print('Could not parse income data')
        sock.close()
        return False

    if to_stop(data):
        sock.close()
        # that's a real STOP according task, all others - loggers
        print('STOP')
        return False

    return True


def main():
    while True:
        if not serve_conn(s):
            break


if __name__ == '__main__':
    main()


