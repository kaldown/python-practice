import unittest

from calculations import eval_rpn


class TestStringMethods(unittest.TestCase):

    def test_success(self):
        ex = "4 5 + 8 * 3 2 / 15 + -"
        # (4+5)*x-(y/2+15)
        self.assertEqual(55.5, eval_rpn(ex))

    def test_notoken(self):
        # no pow operation
        ex = "3 4 2 * 1 5 - 2 ^ / +"
        #3 + 4 * 2 / (1 − 5)^2
        with self.assertRaises(Exception) as cm:
            eval_rpn(ex)
        self.assertEqual(str(cm.exception), 'Wrong token ^')

    def test_fail(self):
        # bad expression
        ex = '1 +'
        with self.assertRaises(Exception) as cm:
            eval_rpn(ex)
        self.assertEqual(str(cm.exception), 'Bad expression %s' % ex)

    def test_empty(self):
        # wrong token
        ex = ''
        with self.assertRaises(Exception) as cm:
            eval_rpn(ex)
        self.assertEqual(str(cm.exception), 'Wrong token %s' % ex)


if __name__ == '__main__':
    unittest.main()
