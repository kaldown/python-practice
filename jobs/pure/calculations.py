import operator


def eval_rpn(expression):
    """Evaluate an expression written in RPN

    Example:
        4 5 + 8 * 3 2 / 15 + -
        returns 55.5

    type expression: string
    rtype: int
    """
    operation_mapper = {
        '+': operator.add,
        '-': operator.sub,
        '*': operator.mul,
        '/': operator.truediv,
    }
    stack = []
    for token in expression.split(' '):
        if token in operation_mapper:
            if len(stack) < 2:
                raise Exception('Bad expression %s' % expression)
            right = stack.pop()
            left = stack.pop()
            stack.append(
                operation_mapper[token](left, right),
            )
        elif token.isnumeric():
            stack.append(int(token))
        else:
            raise Exception('Wrong token %s' % token)

    return stack.pop()
