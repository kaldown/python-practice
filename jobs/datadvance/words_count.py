from collections import Counter
from operator import itemgetter


def count_words(filename):
    """Returns list of words with their occurrences count.
    1) Sorted desc.
    2) Alphabetically when there is an equal words count, ignoring register.

    rtype: list(tuple(str, int))
    """
    with open(filename, 'r') as afile:
        buff = Counter()
        for line in afile:
            buff.update(line.split())

        # words with the same occurrences count must be ordered alphabetically
        # guess it means ignore register
        alpha_sorted = sorted(
                    buff.items(),
                    key=lambda elem: itemgetter(0)(elem).lower(),
                )
        result = sorted(alpha_sorted, key=itemgetter(1), reverse=True)
    return result


def output_words_count(words_list):
    """Print counted words in a following template:
    '<word>: <frequency>'

    rtype: None
    """
    for word, frequency in words_list:
        print(f'{word}: {frequency}')

words_counted = count_words('text.txt')
output_words_count(words_counted)

