#!/usr/bin/python

from math import sqrt

class Vector:
    def __init__(self, *args):
        self._args = args

    def __add__(self, other):
        arr = zip(self._args, other._args)
        res = map(sum, arr)
        return Vector(*res)

    def __str__(self):
        return '{}'.format(self._args)
    
    def __abs__(self):
        base = [pow(n, 2) for n in self._args]
        res = sqrt(sum(base))
        return res

